//
//  NewTeamViewController.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
	
	var chosenSchool: School!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBOutlet weak var nameTF: UITextField!
	@IBOutlet weak var student1TF: UITextField!
	@IBOutlet weak var student2TF: UITextField!
	@IBOutlet weak var student3TF: UITextField!
	
	@IBAction func saveBN(_ sender: Any) {
		chosenSchool.addTeam(name: nameTF.text!, students: [
			student1TF.text!,
			student2TF.text!,
			student3TF.text!])
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelBN(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
}
