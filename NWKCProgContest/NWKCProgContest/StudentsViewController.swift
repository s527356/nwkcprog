//
//  StudentsViewController.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
	
	var students: [String]!
	var teamName: String!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBOutlet weak var titleBR: UINavigationItem!
	@IBOutlet weak var student1LBL: UILabel!
	@IBOutlet weak var student2LBL: UILabel!
	@IBOutlet weak var student3LBL: UILabel!
	
	override func viewWillAppear(_ animation: Bool) {
		student1LBL.text = students[0]
		student2LBL.text = students[1]
		student3LBL.text = students[2]
		titleBR.title = teamName
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
