//
//  NewSchoolViewController.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
	}
	
	@IBOutlet weak var nameTF: UITextField!
	@IBOutlet weak var coachTF: UITextField!
	
	@IBAction func saveBN(_ sender: Any) {
		Schools.shared.addSchool(school: School(name: nameTF.text!, coach: coachTF.text!))
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func cancelBN(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
