//
//  Team.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import Foundation

class Team {
	var name: String
	var students: [String]
	init(name: String, students: [String]) {
		self.name = name
		self.students = students
	}
}
