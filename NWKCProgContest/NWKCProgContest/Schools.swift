//
//  Schools.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import Foundation

class Schools {
	public static var shared = Schools()
	private var schools: [School] = []
	
	func numSchools() -> Int {
		return schools.count
	}
	
	func addSchool(school: School) {
		schools.append(school)
	}
	
	func deleteSchool(at index: Int) {
		schools.remove(at: index)
	}
	
	subscript(index: Int) -> School {
		return schools[index]
	}
	
	private init() {}
}
