//
//  TeamsTableViewController.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class TeamsTableViewController: UITableViewController {
	var school: School!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBOutlet weak var titleBR: UINavigationItem!
	
	override func viewWillAppear(_ animated: Bool) {
		tableView.reloadData()
		titleBR.title = school.name
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return school.teams.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath)
		cell.textLabel?.text = school.teams[indexPath.row].name
		return cell
	}
	

	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			school.teams.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "teamsToStudents" {
			let studentView = segue.destination as! StudentsViewController
			studentView.students = school.teams[tableView.indexPathForSelectedRow!.row].students
			studentView.teamName = school.teams[tableView.indexPathForSelectedRow!.row].name
		} else if segue.identifier == "teamsToNewTeam" {
			let newTeam = segue.destination as! NewTeamViewController
			newTeam.chosenSchool = self.school
		}
	}
}
