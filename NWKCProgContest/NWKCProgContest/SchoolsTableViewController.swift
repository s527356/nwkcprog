//
//  SchoolsTableViewController.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		tableView.reloadData()
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return Schools.shared.numSchools()
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath)
		cell.textLabel?.text = Schools.shared[indexPath.row].name
		cell.detailTextLabel?.text = Schools.shared[indexPath.row].coach
		return cell
	}
	
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			Schools.shared.deleteSchool(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)
		}
	}
	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "schoolsToTeams" {
			let teams = segue.destination as! TeamsTableViewController
			teams.school = Schools.shared[tableView.indexPathForSelectedRow!.row]
		}
	}
}
