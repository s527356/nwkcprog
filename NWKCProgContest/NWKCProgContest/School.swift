//
//  School.swift
//  NWKCProgContest
//
//  Created by Keenan Piveral-Brooks on 3/13/19.
//  Copyright © 2019 Keenan Piveral-Brooks. All rights reserved.
//

import Foundation

class School {
	var name: String
	var coach: String
	var teams: [Team] = []
	
	init(name: String, coach: String) {
		self.name = name
		self.coach = coach
		self.teams = []
	}
	
	func addTeam(name: String, students: [String]) {
		teams.append(Team(name: name, students: students))
	}
}
